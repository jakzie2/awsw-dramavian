#A common file is a great way to store variables and image definitions. It doesn't have to include "common" in the file name, however.

init python:
    #Everything in this block runs when the game starts up (A.K.A init time). We include "python" as well so there isn't a need for a "$" in front of every line

    #For variables we are using ml and dram as a unique namespace
    ml_dram_ask_about_job = False
    ml_dram_ask_about_home = False
    ml_dram_dramavian_mood = 0

    #Declaring persistent variables is a bit different. We only want to create the variable if it doesn't already exist, otherwise pass over it
    #Note that persistent variables are variables that are constant throughout the game
    if not persistent.ml_dram_dramavian_route_ending:
        persistent.ml_dram_dramavian_route_ending = False


image dramavian smile = "cr/dramavian_smile.png"