label ml_dramavian_route_d2:
    show black with dissolvemed
    $ renpy.pause(0.5)
    hide black with dissolvemed
    show dramavian normal with dissolve
    Dr "..."
    c "Okay. Cool."
    Dr "..."
    c "I'm not sure I understand, but I am starting to feel it."
    Dr "..."

    menu:
        "[[Ask about his job.]":
            $ ml_dram_ask_about_job = True
            c "So, what do you do for work?"
        
        "[[Ask about where he lives.]":
            $ ml_dram_ask_about_home = True
            c "So, where do you live?"
            Dr "..."
            c "Do you live at the park where I met you?"
        "[[Don't ask him a question.]":
            pass
    
    Dr "..."
    c "..."
    Dr "..."
    c "Hmmmm..."
    if ml_dram_ask_about_job:
        c "Are you a mime?"
        Dr "..."
        c "Well, if you aren't you totally should be."
    elif ml_dram_ask_about_home:
        c "I guess that was a dumb guess."
        Dr "..."
        c "..."
        Dr "..."
        c "So maybe you have a house?"
        Dr "..."
    else:
        Dr "..."
        c "..."
        Dr "..."
    
    Dr "..."
    c "Good talk."
    Dr "..."

    jump ml_date_end 
