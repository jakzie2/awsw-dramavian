label ml_dramavian_route_d3:
    show black with dissolvemed
    $ renpy.pause(0.5)
    hide black with dissolvemed
    show dramavian normal with dissolve
    Dr "..."
    c "Hello again."
    Dr "..."
    
    menu:
        "You look good today.":
            $ ml_dram_dramavian_mood += 1
            c "I must say, your scales are especially orange today."
            Dr smile "..."
        
        "Nice weather today.":
            c "The weather outside today is very nice. It's... well, warm."
            Dr "..."
            c "Not too cold and not too hot if I say so myself."
            Dr "..."
        
        "You like music?":
            c "Do you like music?"
            Dr "..."
            c "..."
            Dr "..."
            c "You seem like the person who would rather be in a quiet room."
            Dr "..."
    
    c "..."
    Dr normal "..."

    menu:
        "Did you consider my job suggestion?" if ml_dram_ask_about_job:
            c "So, uh, did you consider my idea of becoming a mime."
            Dr "..."
            c "Yes?"
            Dr "..."
            c "I'm going to take that as a yes."
            Dr "..."
        
        "Give him a job suggestion" if not ml_dram_ask_about_job:
            c "You know, you would make a really good mime."
            Dr "..."
            c "See? You're perfect for the job."
            Dr "..."
            c "..."
            Dr "..."
        
        "Compliment his collar.":
            $ ml_dram_dramavian_mood += 1
            c "Your collar is very... {w}shiny."
            Dr smile "..."
            c "Do you polish it?"
            Dr "..."
            c "..."
            Dr "..."
        
        "Discuss the dragon world.":
            c "So... your world is pretty neat."
            Dr "..."
            c "You've got a lot of cool people and places around here."
            Dr "..."
            c "Do you have any thoughts on the portal?"
            Dr "..."
            c "Same."
            Dr "..."
    
    c "Why am I still asking you questions?"
    if persistent.ml_dram_dramavian_route_ending: #If you have run through this mod once before
        Dr normal "Dot Dot Dot{w=0.2}{nw}"
        Dr "..."
        c "What was that?"
        Dr "..."
        c "..."
        Dr "..."
        c "I'm sure I heard you say something."
    else:
        Dr normal "..."
        c "..."
    Dr "..."
    c "Whatever. Good talk."        

    jump ml_date_end 
