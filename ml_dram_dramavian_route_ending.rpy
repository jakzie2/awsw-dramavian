label ml_dramavian_route_ending:
    show black with dissolvemed
    $ renpy.pause(0.5)
    scene viewingspot with dissolvemed
    c "Hey. I got your, uh, note."
    show dramavian normal with dissolve
    Dr "..."
    c "This is the spot, right? For viewing the fireworks?"
    Dr "..."
    c "..."
    play soundloop "fx/fireworks3.ogg" #Loops audio until the soundloop is stopped
    $ renpy.pause(0.5)
    show fireworks at Pan((0, 545), (0, 0), 15.0) with dissolveslow
    $ renpy.pause (6)
    stop music fadeout 2.0
    hide fireworks with dissolveslow
    m "Suddenly, a terrible realization hit me."
    play music "mx/fervor.ogg" fadein 0.5
    c "Oh no. Reza!"
    Dr "...?"
    c "Follow me. We have to go, now!"
    scene black with dissolvemed
    $ renpy.pause (0.5)
    scene np1r at Pan((100, 0), (500, 150), 6.0) with dissolveslow
    $ renpy.pause(5.5)
    show dramavian normal flip with dissolve
    Dr "...!"
    c "What is it?"
    if sebastiansaved == False: #Base game variable for whether you have saved Sebastian
        show sebastiandead at Pan((1080, 608), (300, 150), 10.0) with fade
        $ renpy.pause (8.0)
    else:
        play sound "fx/suitcase.ogg"
        m "Dramavian nudged a suitcase toward me. It had popped open, revealing a few eggs inside. I had no doubt this was Reza's doing."
    c "..."
    Dr "..."
    if sebastiansaved == False:
        hide sebastiandead with fade
    c "It looks like Reza hasn't used the portal yet. Maybe there's still time to stop him."
    c "There's an underground building, under the portal. He must be there, going after the generators inside."
    Dr "..."
    c "Come on. We have to stop him."


    stop soundloop fadeout 3.0
    # stop music fadeout 2.0
    scene black with dissolvemed
    $ renpy.pause (0.5)
    scene hallway at Pan((0, 0), (0, 150), 3.75) with dissolvemed
    $ renpy.pause(3)
    show dramavian normal flip at Position(xpos=0.0,xanchor='center') with dissolve
    play sound "fx/door/hallwaydoor.ogg"
    m "Suddenly, one of the doors opened, and out came Reza, carrying a large cardboard box."
    m "When he spotted us, he set it on the ground and drew his gun."
    play sound "fx/box1.wav"
    show reza gunself at right with dissolve
    Rz "You brought one of them, huh? Didn't even want to give our people a chance?"
    c "..."
    Rz angry "Good. Don't try to deny it. You know what you've done."
    Dr "..."
    Rz gunpoint "You! Who are you, with their police? Are there more outside?"
    Dr "..."
    Rz angry "Answer me!"
    c "..."
    Rz gunself "Why aren't you saying anything?"
    Dr "..."
    Rz angry "Why aren't either of you saying anything?!"
    c "..."
    Dr "..."
    Rz annoyed "What- What are you doing? You're just standing there!"
    Rz angry "You betrayed our people, [player_name]! You think this is going to slow me down?"
    c "..."
    Rz laugh "You're kidding, right?"
    Rz angry "I have a gun! Get out of my way!"

    show dramavian at Position(xpos=-0.3, xanchor='center')
    show reza at Position(xpos=0.45, xanchor='center')
    with ease

    m "While he postured at us, a figure emerged quietly from the shadows in the hallway behind Reza."
    show izumi normal at right with easeinbottom
    As "..."
    play sound "fx/rev.ogg"
    Rz gunself "Last chance, [player_name]. Move or I'll make you and your friend move."
    c "..."
    Rz "So be--"
    show izumi at Position(xpos=0.45, xanchor='center') with ease
    $ renpy.pause(0.5)
    hide reza
    hide izumi
    with easeoutbottom
    play sound ["fx/impact3.ogg", "fx/silence.ogg", "fx/silence.ogg", "fx/gunshots5.ogg", "fx/gunshot2.wav", "fx/trigger2.ogg"]
    m "The figure jumped Reza, yanking his gun arm back toward him and away from us as they fell to the floor."
    m "The gun's chambers emptied between them."
    $ renpy.pause(0.8)
    show izumi normal 1 c with easeinbottom
    m "The administrator's mask had been knocked off in the struggle, but she rose shakily, gun in her hand."
    play sound "fx/rev.ogg"
    $ renpy.pause(0.8)
    play sound "fx/gunshots5.ogg"
    m "After reloading and emptying another five shots into Reza, she dropped the gun."
    stop music fadeout 2.0
    m "Her voice was weak, but audible."
    Iz "So this timeline does work. Sorry, [player_name]."
    hide izumi with easeoutbottom
    play sound "fx/impact3.ogg"
    m "With that, she collapsed."
    play music "mx/prophecy.ogg" fadein 1.0
    show izumiinjured3 at Pan((300, 0), (600, 608), 7.0) with fade
    $ renpy.pause(5.0)
    hide izumiinjured3
    show rezashotsixtimes at Pan ((1000, 626), (1280,0), 10.0) with fade
    $ renpy.pause(8.5)
    hide rezashotsixtimes with fade
    show dramavian at left with ease
    Dr "..."
    c "..."
    scene black with dissolveslow
    
    nvl clear
    window show

    n "With the help of the underground facility's generators, the dragons were able to divert the comet."
    n "When it came time for me to deliver a report on what had happened, along with Reza's body, I discovered humanity's coordinates had been erased from the portal."
    n "Perhaps that's what the administrator had meant with her apology."

    # n "I was able to find the generators, and they were able to restore the coordinates to their original state." # Wishful thinking by GitHub Copilot

    window hide
    nvl clear

    $ renpy.pause(2.0)

    scene o at Pan((0, 0), (0, 250), 5.0) with dissolveslow
    $ renpy.pause(3.3)
    play sound "fx/door/doorbell.wav"
    $ renpy.pause(2.0)
    stop sound fadeout 0.5
    $ renpy.pause(0.4)
    play sound "fx/door/handle.wav"
    $ renpy.pause(1.0)

    show dramavian normal with dissolve
    Dr "..."
    c "Hey, Dramavian."
    Dr "..."
    c "No. Sorry. I can't do that today. I... I have to tell you..."
    Dr "..."
    c "I need to go back. Back to the day I arrived here, that this all started. It's the only way to save humanity, too."
    Dr "...?"
    c "There are other coordinates in the portal, locked to only my biometric signature. They'll let me go back and try this all again."
    Dr "..."
    c "..."
    Dr "..."
    c "Thanks, Dramavian. I guess I'll see you again on that park bench, huh?"
    Dr "..."
    c "..."
    Dr "..."
    c "..."
    scene black with dissolveslow
    stop music fadeout 2.0
    $ renpy.pause (3.0)
    $ renpy.block_rollback()
    play sound "mx/sweetmemories.ogg"
    show fireworks at Pan ((-960, 545), (-200, 350), 20)
    show credits1 at left
    with dissolvemed
    $ renpy.pause(8.0)
    show black2 at left with dissolvemed
    show credits2 at left with dissolvemed
    $ renpy.pause(8.0)
    scene black with dissolvemed
    show park2 at Pan ( (800, 0), (1000, 608), 20.0)
    show credits3 at right
    with dissolvemed
    $ renpy.pause(8.0)
    show black2 at right with dissolvemed
    show credits4 at right with dissolvemed
    $ renpy.pause(8.0)
    scene black with dissolvemed

    show closed Pan ((740, 608), (1240, 0), 24)
    show credits7 at right
    with dissolvemed

    $ renpy.pause(8.0)

    show black2 at right with dissolvemed
    show credits7 at right with dissolvemed
    $ renpy.pause(8.0)

    scene black with dissolvemed
    show dramavian normal at right
    show credits9 at left
    with dissolvemed
    $ renpy.pause(8.0)

    scene black with dissolvemed
    scene logo with dissolvemed
    $ renpy.pause(8.5)
    scene black with dissolvemed

    stop music fadeout 2.0
    
    #A bunch of basegame checks that influence variables
    python:
        renpy.pause(4.0)
        persistent.anygoodending = True
        lastendingseen = "good"
        persistent.endingsseen += 1
    
    call izumimask from _call_izumimask_dramavian
    call optimistcheck from _call_optimistcheck_dramavian
    if not persistent.ml_dram_dramavian_route_ending:
        $ persistent.ml_dram_dramavian_route_ending = True
        call syscheck from _call_syscheck_dramavian
        play sound "fx/system.wav"
        s "You have seen the Magmalink example Dramavian good ending!"
    
    if persistent.endingsseen == 1:
        $ persistent.firstending2 = "dramavian"
        jump tut
    else:
        jump mainmenu 
