label ml_dramavian_route_d1:
    show black with dissolvemed
    $ renpy.pause(0.5)
    hide black with dissolvemed
    show dramavian normal with dissolve
    c "Hey. Thanks for coming over."
    Dr "..."
    c "Right. So, can I get you anything? I'm pretty sure my kitchen is well stocked."
    Dr "..."
    c "..."
    Dr "..."
    c "That's a neat looking collar. Very, ah, neck supportive?"
    Dr "..."
    c "Can I see the thing hanging from it?"
    Dr "..."
    c "\"Dramavian\" huh?"
    c "Well, good meeting you Dramavian. Let me know if you want to come over again."
    Dr "..."
    c "..."
    hide dramavian with easeoutleft
    $ renpy.pause(0.5)
    jump ml_date_end 
