from modloader import modast, modinfo
from modloader.modclass import Mod, loadable_mod

@loadable_mod
class AWSWMod(Mod): 
    def mod_info(self):
        return ("Dramavian Example Mod", "0.1.0", "Jakzie, 4onen, Eval")
    
    def mod_load(self):
        ml = modinfo.get_mods()["MagmaLink"].import_ml()
        
        ml.CharacterRoute("ml_dramavian", "Dramavian") \
            .add_date(text="Meet with that quiet dragon.", condition="chap2rested > 0", jump='ml_dramavian_route_d1') \
            .add_date(jump='ml_dramavian_route_d2') \
            .add_date(jump='ml_dramavian_route_d3') \
            .set_ending(jump='ml_dramavian_route_ending') \
            .build()

    
    def mod_complete(self):
        pass